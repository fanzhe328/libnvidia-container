/*
 * Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
 */

#include <stdio.h>

#ifndef HEADER_DRIVER_CFG_H
#define HEADER_DRIVER_CFG_H

#define MAX_LINE 1024
#define CFG_FILE_PATH "/etc/nvidia-container-runtime/libnvidia.conf"

int driver_config_init(void);
int split_first(char *, const char, char *, char *);
int driver_get_rm_version_from_cfg(char *);
int driver_get_cuda_version_from_cfg(char *);
unsigned driver_get_device_count_from_cfg(void);
int driver_get_device_busid_from_cfg(unsigned int, char *);
int driver_get_device_uuid_from_cfg(unsigned int, char *);
int driver_get_device_model_from_cfg(char *);
int driver_get_device_brand_from_cfg(char *);
int driver_get_device_arch_from_cfg(unsigned int *, unsigned int *);
int isspacestr(char*);

#endif //HEADER_DRIVER_CFG_H
