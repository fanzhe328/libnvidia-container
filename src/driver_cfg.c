/*
 * Copyright (c) 2017-2018, NVIDIA CORPORATION. All rights reserved.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>

#include "driver_cfg.h"
#include "utils.h"

const char *NVRM_VERSION_KEY = "NVRM version";
const char *CUDA_VERSION_KEY = "CUDA version";
const char *DEVICE_INDEX_KEY = "Device Index";
const char *BUS_ID_KEY = "Bus Location";
const char *GPU_UUID_KEY = "GPU UUID";
const char *MODEL_KEY = "Model";
const char *BRAND_KEY = "Brand";
const char *ARCH_KEY = "Architecture";
const char DELIM = ':';

char **cfg;
size_t cfg_len;

int
isspacestr(char* str)
{
    int rv = 0;
    for(size_t i=0; i<strlen(str); ++i) {
        if(isspace(str[i]))
            continue;
        break;
    }
    return rv;
}

int
driver_config_init()
{
    FILE *fp;
    if ((fp = fopen(CFG_FILE_PATH, "r")) == NULL)
    {
        log_infof("config file '%s' not exist.", CFG_FILE_PATH);
        return -1;
    }

    char buf[MAX_LINE];
    cfg = malloc(sizeof(char *) * MAX_LINE);
    char **p = cfg;
    cfg_len = 0;
    while (fgets(buf, MAX_LINE, fp) != NULL)
    {
        if(isspacestr(buf))
            continue;
        char *line = malloc(sizeof(char) * MAX_LINE);
        size_t len = strlen(buf);
        strncpy(line, buf, len);
        if (line[len - 1] == '\n')
        {
            if (strlen(line) == 1)
            {
                continue;
            }
            line[len - 1] = '\0';
        }
        else
        {
            line[len] = '\0';
        }
        *p++ = line;

        cfg_len++;
    }
    fclose(fp);
    if(cfg_len == 0) 
        return -1;
    return 0;
}

int
split_first(char *src, const char delim, char *key, char *value)
{
    size_t len = strlen(src);
    char *p = src;
    size_t count = 0, i = 0;
    if (len <= 2)
        return -1;

    for (; i < len; ++i)
    {
        count++;
        if (src[i] == delim)
            break;
    }
    if (i == len)
        return -1;

    strncpy(key, p, count - 1);
    key[count - 1] = '\0';
    count = 0;

    while (isspace(src[++i]))
        ;
    p = &src[i];
    for (; i < len; ++i)
    {
        count++;
        if (src[i] == '\n')
        {
            count--;
            break;
        }
        if (src[i] == '\0')
            break;
    }
    strncpy(value, p, count);
    value[count] = '\0';
    return 0;
}

int
driver_get_rm_version_from_cfg(char *rm_version)
{
    char **p = cfg;
    int ret = -1;
    char *key = malloc(sizeof(char) * MAX_LINE);
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, rm_version)==0)
        {
            if (strncmp(key, NVRM_VERSION_KEY, strlen(NVRM_VERSION_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }
    free(key);
    return ret;
}

int
driver_get_cuda_version_from_cfg(char *cuda_version)
{
    char **p = cfg;
    int ret = -1;
    char *key = malloc(sizeof(char) * MAX_LINE);
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, cuda_version)==0)
        {
            if (strncmp(key, CUDA_VERSION_KEY, strlen(CUDA_VERSION_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }
    free(key);
    return ret;
}

unsigned
driver_get_device_count_from_cfg()
{
    char **p = cfg;
    char *key = malloc(sizeof(char) * MAX_LINE);
    char *val = malloc(sizeof(char) * MAX_LINE);
    unsigned device_count = 0;
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, val)==0)
        {
            if (strncmp(key, DEVICE_INDEX_KEY, strlen(DEVICE_INDEX_KEY)) == 0)
                device_count++;
        }
        ++p;
    }
    free(key);
    free(val);
    return device_count;
}

int
driver_get_device_busid_from_cfg(unsigned int idx, char *busid)
{
    char **p = cfg;
    char *key = malloc(sizeof(char) * MAX_LINE);
    int ret = -1;
    size_t i = 0;
    for (i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, busid)==0)
        {
            if (strncmp(key, DEVICE_INDEX_KEY, strlen(DEVICE_INDEX_KEY)) == 0)
            {
                if (idx == (unsigned)atoi(busid))
                    break;
            }
        }
        ++p;
    }

    for (; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, busid)==0)
        {
            if (strncmp(key, BUS_ID_KEY, strlen(BUS_ID_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }

    free(key);
    return ret;
}

int
driver_get_device_uuid_from_cfg(unsigned int idx, char *uuid)
{
    char **p = cfg;
    char *key = malloc(sizeof(char) * MAX_LINE);
    int ret = -1;
    size_t i = 0;
    for (i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, uuid)==0)
        {
            if (strncmp(key, DEVICE_INDEX_KEY, strlen(DEVICE_INDEX_KEY)) == 0)
            {
                if (idx == (unsigned)atoi(uuid))
                    break;
            }
        }
        ++p;
    }

    for (; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, uuid)==0)
        {
            if (strncmp(key, GPU_UUID_KEY, strlen(GPU_UUID_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }

    free(key);
    return ret;
}

int
driver_get_device_model_from_cfg(char *model)
{
    char **p = cfg;
    int ret = -1;
    char *key = malloc(sizeof(char) * MAX_LINE);
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, model)==0)
        {
            if (strncmp(key, MODEL_KEY, strlen(MODEL_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }
    free(key);
    return ret;
}

int
driver_get_device_brand_from_cfg(char *brand)
{
    char **p = cfg;
    int ret = -1;
    char *key = malloc(sizeof(char) * MAX_LINE);
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, brand)==0)
        {
            if (strncmp(key, BRAND_KEY, strlen(BRAND_KEY)) == 0)
            {
                ret = 0;
                break;
            }
        }
        ++p;
    }
    free(key);
    return ret;
}

int
driver_get_device_arch_from_cfg(unsigned int *major, unsigned int *minor)
{
    char **p = cfg;
    int ret = -1;
    char *key = malloc(sizeof(char) * MAX_LINE);
    char *arch = malloc(sizeof(char) * MAX_LINE);
    for (size_t i = 0; i < cfg_len; ++i)
    {
        if (split_first(*p, DELIM, key, arch)==0)
        {
            if (strncmp(key, ARCH_KEY, strlen(ARCH_KEY)) == 0)
            {
                ret = 1;
                break;
            }
        }
        ++p;
    }

    char *val = malloc(sizeof(char) * MAX_LINE);
    if (split_first(arch, '.', key, val)==0)
    {
        *major = (unsigned)atoi(key);
        *minor = (unsigned)atoi(val);
        ret = 0;
    }
    free(key);
    free(val);
    free(arch);
    return ret;
}
